package studentdatabaseapp;

import java.util.Scanner;

public class StudentDatabaseApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Enter number of student to add: ");
		int numOfStudent = in.nextInt();
		
		Student [] students = new Student[numOfStudent];
		
		for(int i = 0; i < numOfStudent; i++) {
			students[i] = new Student();
			students[i].enroll();
			students[i].showStudentInfo();
			students[i].payTution();
			System.out.print("Your pending Balance : $"+ students[i].getBalance());
		}
	}

}

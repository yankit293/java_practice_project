package studentdatabaseapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Student {

	private String firstName;
	private String lastName;
	private int gradeYear;
	private String studentId;
	private int id = 1000;
	private int tuitionBalance = 0;
	private int costOfCourse = 600;
	private ArrayList<String> courses = new ArrayList<String>(List.of("History 101", "Mathematics 101", "English 101", "Chemistry 101", "Computer Science 101"));
	private ArrayList<String> enrolledCourses = new ArrayList<String>();
	
	//constructor to get user info
	public Student() {
		Scanner in = new Scanner(System.in);
		System.out.print("\nEnter your First Name: ");
		this.firstName = in.nextLine();
		System.out.print("Enter your Last Name: ");
		this.lastName = in.nextLine();
		System.out.print("1 - Freshmen\n2 - sophenore\n3 - Junior\n4 - Senior\n Enter your grade level: ");
		this.gradeYear = in.nextInt();
		setStudentId();
		//System.out.print("Display Name: "+firstName +" "+ lastName + "\n Student ID: "+studentId + "\n");
	}
	
	//generate unique id
	private void setStudentId() {
		id++;
		this.studentId = gradeYear + "" + id;
	}
	//enroll in course
	public void enroll() {
		Scanner in = new Scanner(System.in);
		do {
		for(int i = 0; i< courses.size(); i++) {
			System.out.println(i + " - " +courses.get(i));
		}
		System.out.print("9 - Exit\nChoose your courses: ");
		int index = in.nextInt();
		if(index != 9) {
			if(!enrolledCourses.contains(courses.get(index))) {
				enrolledCourses.add(courses.get(index));
				tuitionBalance = tuitionBalance + costOfCourse;
			}
			else 
				System.out.println("This Course is Already Added!");
		}
			
		else
			break;
		
		}while(1!=0);
		//System.out.println(enrolledCourses + "\n Total Tuition Balance: " + tuitionBalance);
	}
	
	//view balance
	public int getBalance() {
		return this.tuitionBalance;
	}
	
	//Pay tuition balance
	public void payTution() {
		System.out.println("\nYour payable amount is $" + getBalance());
		Scanner in = new Scanner(System.in);
		System.out.println("\nHow much you want to pay ?: $");
		int amount = in.nextInt();
		if(amount<tuitionBalance)
			tuitionBalance = tuitionBalance - amount;
		else
			System.out.println("You entered $" + (tuitionBalance - amount) + "more then your pending Balance" );
	}
	// show Student info
	public void showStudentInfo() {
		System.out.print("Student Name: "+ firstName + " " + lastName +
				"\nStudent ID: "+ studentId + "\nEnrolled Courses: " + enrolledCourses +
				"\nTution Balance: $" + getBalance()
				);
	}
	
}

package Email;

import java.util.Scanner;

public class EmailInfo {
	private String fname;
	private String lname;
	private String dept;
	private String companySuffix = "naehas.com";
	private String email;
	private String password;
	private String alternateEmail;
	private int defaultPasswordLength = 8;
	private int mailBoxCapacity = 200;
	
	public EmailInfo(String fname, String lname) {
		this.fname = fname;
		this.lname = lname;
		//System.out.println("First Name:"+fname+" Last Name:"+lname);
		this.dept = setDepartment();
		this.password = setPassword(defaultPasswordLength);
		//System.out.println("Department:"+dept+"\nPassword:"+password);
		this.email = fname.toLowerCase()+"."+lname.toLowerCase()+"@"+dept+"."+companySuffix;
		//System.out.println("Your Email: "+email);
	}
	
	private String setDepartment() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose your department.\n1 for Slaes\n2 for Development\n3 for Accounting\n0 for None");
		int deptId;
		if(sc.hasNextInt())
		deptId = sc.nextInt();
		else {
			System.out.println("Enter a Integer value as Choice: ");
			sc.nextLine();
			deptId = sc.nextInt();
		}
		if(deptId == 1) return "sales";
		else if(deptId == 2) return "development";
		else if(deptId == 3) return "accounting";
		else return "";
	}
	
	private String setPassword(int length) {
		String possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*";
		char [] password = new char[length];
		for(int i = 0; i < length; i++) {
			int rnum = (int)(Math.random()*possibleLetters.length());
			//System.out.println(rnum);
			password[i] = possibleLetters.charAt(rnum);
		}
		return new String(password);
		
	}
	//setters
	public void setAlternateEmail(String altrEmail) { this.alternateEmail = altrEmail; }
	public void setMailBoxCapacity(int capacity) { this.mailBoxCapacity = capacity; }
	public void changePassword(String password) { this.password = password; }
	
	//getters
	public String getEmail() { return email; }
	public String getAlternateEmail() { return alternateEmail; }
	public String getPassword() { return password; }
	public int getMailBoxCapacity() { return mailBoxCapacity; }
	public String getFullName() { return fname + " " + lname; }
	
}

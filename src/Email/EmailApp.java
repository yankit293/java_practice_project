package Email;
import java.io.IOException;
import java.util.*;
public class EmailApp {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your First Name: ");
		String firstName = sc.nextLine();
		System.out.print("Enter your Last Name: ");
		String lastName = sc.nextLine();
		EmailInfo employee = new EmailInfo(firstName, lastName);
		int choice;
		do {
			System.out.println("\n1 to Get Full Name of employee.\n2 to check employee email."
					+"\n3 to check MailBox Capacity.\n4 to Change Password.\n5 to set Mailbox Capacity.\n6 to Change alternate email.\n 0 to Exit.\nEnter your Choice:");
			if(sc.hasNextInt())
			choice = sc.nextInt();
			else {
				System.out.println("Enter a Integer Value as Choice: ");
				sc.nextLine();
				choice = sc.nextInt();
				}
			switch(choice) {
			case 0: choice = 0; break;
			case 1: System.out.println(employee.getFullName()); break;
			case 2: System.out.println(employee.getEmail()); break;
			case 3: System.out.println(employee.getMailBoxCapacity()); break;
			case 4: System.out.print("Enter your new password: ");
					sc.nextLine();
					String password = sc.nextLine();
					employee.changePassword(password);
					break;
			case 5: System.out.println("Enter your new Capacity: ");
					sc.nextLine();
					int capacity = sc.nextInt();
					employee.setMailBoxCapacity(capacity);
					break;
			case 6: System.out.println("Enter new Alternative Email: ");
					sc.nextLine();
					String atlEmail = sc.nextLine();
					employee.setAlternateEmail(atlEmail);
					break;
			default: System.out.println("Wrong Choice!");
			}
		}
		while(choice!=0);

	}
}